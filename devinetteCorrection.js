
onload = init;

const motAtrouver = "DWWM";
function init(){
    let divLettre = document.getElementById('alphabet').textContent = '';

    let divNbEssai = document.getElementById('iTentative').innerText;
    nbEssai = 5;
    document.getElementById('iTentative').innerText= nbEssai;

    //code lettre ASCII
    for (let i = 65; i<=90; i++){
        const lettre = String.fromCharCode(i);
        const bouton = document.createElement('button');
        bouton.textContent = lettre;
        bouton.addEventListener(
            'click',
            ()=>clicSurLaLettre(lettre)
        );
        document.getElementById('alphabet').appendChild(bouton);
    }
    document.getElementById('motAtrouver').textContent = motAtrouver.replace(/\S/g, '_');
}

let boutonRecommencer = document.getElementById('irecommence');
boutonRecommencer.addEventListener('click', init);



function clicSurLaLettre(lettre){
    console.log("J'ai cliqué sur la lettre " + lettre)
    if(motAtrouver.includes(lettre)){
        afficherLettre(lettre);
    }else {
        let nbEssai = document.getElementById('iTentative').innerText;
        nbEssai--;
        document.getElementById('iTentative').innerText= nbEssai;
    }
}


function afficherLettre(lettre){
    //Je prend le mot dans le html
    let motDom = document.getElementById('motAtrouver').textContent.split('');
    //je boucle sur chaque lettre du mot
    for (let i = 0; i < motAtrouver.length; i++) {
        // si c'est la bonne lettre je remplace le '_ par la lettre
        if(motAtrouver[i]=== lettre){
            motDom[i] = lettre;
        }
    }
    //je met à jour le dom
    document.getElementById('motAtrouver').textContent = motDom.join('');
}

//Méthode Elodie et Alexis
//const motCache=[];
//for (const lettre of motAtrouver) {
 //   motCache.push('_') ;
//}


